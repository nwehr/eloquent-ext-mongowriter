//
// Copyright 2013-2014 EvriChart, Inc. All Rights Reserved.
// See LICENSE.txt
//

// C
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cstdlib>

// C++
#include <string>
#include <mutex>
#include <sstream>

// Boost
#include <boost/property_tree/ptree.hpp>

// Mongo


// Internal
#include "MongoWriter.h"

///////////////////////////////////////////////////////////////////////////////
// RestWriter : IO
///////////////////////////////////////////////////////////////////////////////
Eloquent::MongoWriter::MongoWriter( const boost::property_tree::ptree::value_type& i_Config
								   , std::mutex& i_QueueMutex
								   , std::condition_variable& i_QueueCV
								   , std::queue<QueueItem>& i_Queue
								   , unsigned int& i_NumWriters )
: IO( i_Config, i_QueueMutex, i_QueueCV, i_Queue, i_NumWriters )
, m_Connection()
, m_Host( m_Config.second.get_optional<std::string>( "host" ) )
, m_Collection( m_Config.second.get_optional<std::string>( "collection" ) )
{}

Eloquent::MongoWriter::~MongoWriter() {}

void Eloquent::MongoWriter::operator()() {
	try {
		if( !m_Host.is_initialized() ) {
			throw std::runtime_error( "host is not specified" );
		}
		
		if( !m_Collection.is_initialized() ) {
			throw std::runtime_error( "collection is not specified" );
		}
		
		// Establish a connection with the database
		m_Connection.connect( m_Host.get() );
		
		while( true ) {
			QueueItem& Item = NextQueueItem();
			
			try {
				if( !m_Connection.isStillConnected() ) {
					throw std::runtime_error( "lost connection to database" );
				}
				
				m_Connection.insert( m_Collection.get(), mongo::fromjson( Item.Data() ) );
				
			} catch( mongo::DBException &e ) {
				syslog( LOG_ERR, "%s #MongoWriter::operator()() #Error", e.what() );
			}
			
		}
		
	} catch( mongo::DBException &e ) {
		syslog( LOG_ERR, "%s #MongoWriter::operator()() #Error", e.what() );
	} catch( std::exception& e ) {
		syslog( LOG_ERR, "%s #MongoWriter::operator()() #Error", e.what() );
	} catch( ... ) {
		syslog( LOG_ERR, "unknown exception #MongoWriter::operator()() #Error" );
	}
	
}
