#ifndef _RestWriter_h
#define _RestWriter_h

//
// Copyright 2013-2014 EvriChart, Inc. All Rights Reserved.
// See LICENSE.txt
//

// C++
#include <iostream>

// Mongo
#include <mongo/client/dbclient.h>

#define MONGO_EXPOSE_MACROS

#include <mongo/util/assert_util.h>
#include <mongo/bson/bsonobj.h>
#include <mongo/db/json.h>

// Internal
#include "Eloquent/Extensions/IO/IO.h"

namespace Eloquent {
	///////////////////////////////////////////////////////////////////////////////
	// MongoWriter : IO
	///////////////////////////////////////////////////////////////////////////////
	class MongoWriter : public IO {
		MongoWriter();
		
	public:
		explicit MongoWriter( const boost::property_tree::ptree::value_type& i_Config
							, std::mutex& i_QueueMutex
							, std::condition_variable& i_QueueCV
							, std::queue<QueueItem>& i_Queue
							, unsigned int& i_NumWriters );

		virtual ~MongoWriter();
		virtual void operator()();

	private:
		mongo::DBClientConnection m_Connection;
		
		boost::optional<std::string> m_Host;
		boost::optional<std::string> m_Collection;
		
	};
}

#endif // _RestWriter_h
