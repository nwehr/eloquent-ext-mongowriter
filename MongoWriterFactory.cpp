//
// Copyright 2013-2014 EvriChart, Inc. All Rights Reserved.
// See LICENSE.txt
//

// Internal
#include "MongoWriter.h"
#include "MongoWriterFactory.h"

///////////////////////////////////////////////////////////////////////////////
// MongoWriterFactory : IOExtensionFactory
///////////////////////////////////////////////////////////////////////////////
Eloquent::MongoWriterFactory::MongoWriterFactory() {}
Eloquent::MongoWriterFactory::~MongoWriterFactory() {}
	
Eloquent::IO* Eloquent::MongoWriterFactory::New( const boost::property_tree::ptree::value_type& i_Config
														, std::mutex& i_QueueMutex
														, std::condition_variable& i_QueueCV
														, std::queue<QueueItem>& i_Queue
														, unsigned int& i_NumWriters )
{
	return new MongoWriter( i_Config, i_QueueMutex, i_QueueCV, i_Queue, i_NumWriters );
}